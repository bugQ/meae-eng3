inline Model::Model(Mesh & mesh, Material & mat, Vector3 position, Vector3 scale)
	: mesh(&mesh), mat(&mat), position(position), scale(scale)
{
}
