#include "Vector4.h"

namespace eae6320
{
	const Vector4 Vector4::Zero = Vector4(0, 0, 0, 0);
	const Vector4 Vector4::One = Vector4(1, 1, 1, 1);
	const Vector4 Vector4::I = Vector4(1, 0, 0, 0);
	const Vector4 Vector4::J = Vector4(0, 1, 0, 0);
	const Vector4 Vector4::K = Vector4(0, 0, 1, 0);
}