#include "Vector3.h"

namespace eae6320
{
	const Vector3 Vector3::Zero = Vector3(0, 0, 0);
	const Vector3 Vector3::One = Vector3(1, 1, 1);
	const Vector3 Vector3::I = Vector3(1, 0, 0);
	const Vector3 Vector3::J = Vector3(0, 1, 0);
	const Vector3 Vector3::K = Vector3(0, 0, 1);
}