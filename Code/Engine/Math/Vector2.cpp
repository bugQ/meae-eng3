#include "Vector2.h"

namespace eae6320
{
	const Vector2 Vector2::Zero = Vector2(0, 0);
	const Vector2 Vector2::One = Vector2(1, 1);
	const Vector2 Vector2::I = Vector2(1, 0);
	const Vector2 Vector2::J = Vector2(0, 1);
}